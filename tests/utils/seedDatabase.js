import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import prisma from '../../src/prisma'

const userOne = {
  input: {
    name: "dummy",
    email: "dummy@example.com",
    password: bcrypt.hashSync('dummy')
  },
  user: undefined,
  jwt: undefined
}

const userTwo = {
  input: {
    name: "dummy2",
    email: "dummy2@example.com",
    password: bcrypt.hashSync('dummy2')
  },
  user: undefined,
  jwt: undefined
}

const seedDatabase = async () => {
  // Delete test data
  await prisma.mutation.deleteManyUsers()

  // Create user one
  userOne.user = await prisma.mutation.createUser({
      data: userOne.input
  })

  // set jwt
  userOne.jwt = jwt.sign({ userId: userOne.user.id }, process.env.JWT_SECRET)

  // Create user two
  userTwo.user = await prisma.mutation.createUser({
    data: userTwo.input
  })

  // set jwt
  userTwo.jwt = jwt.sign({ userId: userTwo.user.id }, process.env.JWT_SECRET)
}

export { seedDatabase as default, userOne, userTwo }